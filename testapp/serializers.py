from django.contrib.auth.models import User, Group
from rest_framework import serializers
from testapp import models


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id','username', 'email')


class IncomeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Income
        fields = ('id','nombre', 'monto','usuario')

class OutcomeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Outcome
        fields = ('id','nombre', 'monto','usuario')

class Savingsserializer(serializers.ModelSerializer):
    class Meta:
        model = Savings
        fields = ('id', 'nombre', 'monto','usuario')
