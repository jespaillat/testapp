from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth import authenticate

# Objetos app

class User(models.Model):
    username = models.CharField(max_length=200)
    email = models.EmailField(max_length=254)


class Income(models.Model):
    nombre = models.CharField(max_length=200)
    monto = models.DecimalField(max_digits=19, decimal_places=2)
    usuario = models.ForeignKey(User)


class Outcome(models.Model):
    nombre = models.CharField(max_length=200)
    monto = models.DecimalField(max_digits=19, decimal_places=2)
    usuario = models.ForeignKey(User)
    

class Savings(models.Model):
    nombre = models.CharField(max_length=200)    
    monto = models.DecimalField(max_digits=19, decimal_places=2)
    usuario = models.ForeignKey(User)